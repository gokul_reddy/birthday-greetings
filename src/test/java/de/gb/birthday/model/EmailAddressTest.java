package de.gb.birthday.model;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class EmailAddressTest
{
	@Test
	public void hasValueItWasCreatedWith()
	{
		String address = "jane.doe@foobar.com";
		
		EmailAddress emailAddress = EmailAddress.of(address);
		
		assertThat(emailAddress.address(), equalTo(address));
	}
	
	@Test
	public void isEqualToItself()
	{
		String address = "john.doe@foobar.com";
		
		EmailAddress emailAddress = EmailAddress.of(address);
		
		assertThat(emailAddress, equalTo(emailAddress));
	}
	
	@Test
	public void isEqualToSameAddress()
	{
		String address = "jim.doe@foobar.com";
		
		EmailAddress emailAddress = EmailAddress.of(address);
		EmailAddress emailAddress2 = EmailAddress.of(address);
		
		assertThat(emailAddress, equalTo(emailAddress2));
	}
	
	@Test
	public void isNotEqualToDifferentAddress()
	{
		EmailAddress emailAddress1 = EmailAddress.of("jessie.doe@foobar.com");
		EmailAddress emailAddress2 = EmailAddress.of("james.doe@foobar.com");
		
		assertThat(emailAddress1, not(equalTo(emailAddress2)));
	}
	
	@Test
	public void isNotEjqoualsToOtherClassInstance() throws Exception
	{
		EmailAddress emailAddress = EmailAddress.of("joshua.doe@foobar.com");
		
		assertThat(emailAddress, not(equalTo(new Object())));
	}
	
	@Test
	public void hasSameHashcodeAsSameAddress()
	{
		String address = "jenny.doe@foobar.com";
		
		EmailAddress emailAddress = EmailAddress.of(address);
		EmailAddress emailAddress2 = EmailAddress.of(address);
		
		assertThat(emailAddress.hashCode(), equalTo(emailAddress2.hashCode()));
	}
	
	@Test
	public void hasDifferentHashCodeFromDifferentAddress()
	{
		EmailAddress emailAddress1 = EmailAddress.of("jackson.doe@foobar.com");
		EmailAddress emailAddress2 = EmailAddress.of("joshua.doe@foobar.com");
		
		assertThat(emailAddress1.hashCode(), not(equalTo(emailAddress2.hashCode())));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void rejectsBrokenEmailAddressWithoutHostname()
	{
		EmailAddress.of("john.duh@");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void rejectsBrokenEmailAddressWithoutTLD()
	{
		EmailAddress.of("john.duh@foobar");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void rejectsBrokenEmailAddressWithoutUser()
	{
		EmailAddress.of("@foobar.com");
	}
	
	@Test
	public void toStringIsEmailAddress()
	{
		String address = "jennifer.doe@foobar.com";
		
		EmailAddress emailAddress = EmailAddress.of(address);
		
		assertThat(emailAddress.toString(), equalTo(address));
	}
}
