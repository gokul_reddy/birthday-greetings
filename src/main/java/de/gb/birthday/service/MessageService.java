package de.gb.birthday.service;

import de.gb.birthday.model.EmailAddress;

public interface MessageService
{
	void sendGreeting(String subject, String message, EmailAddress emailAddress);
}
