package de.gb.birthday.service;

import static java.text.MessageFormat.format;

import java.time.*;
import java.util.List;

import de.gb.birthday.model.EmployeeRecord;
import de.gb.birthday.repository.EmployeeRepository;

public class BirthdayService
{
	private static final String DEFAULT_SUBJECT = "Happy birthday!";
	private static final String MESSAGE_TEMPLATE = "Happy birthday, dear {0}!";
	
	private static final MonthDay FEBRUARY_28 = MonthDay.of(2, 28);
	
	private EmployeeRepository repository;
	private MessageService emailService;

	public BirthdayService(EmployeeRepository repository, MessageService emailService)
	{
		this.repository = repository;
		this.emailService = emailService;
	}

	public void sendGreetings(LocalDate today)
	{
		List<EmployeeRecord> employees = repository.loadAll();
		
		for(EmployeeRecord employee : employees)
		{
			if(employeeIsBornToday(today, employee) || employeeIsBornInLeapYearTomorrow(today, employee))
			{
				emailService.sendGreeting(DEFAULT_SUBJECT, messageFor(employee), employee.emailAddress());
			}
		}
	}

	private String messageFor(EmployeeRecord employee)
	{
		return format(MESSAGE_TEMPLATE, employee.firstName());
	}

	private boolean employeeIsBornToday(LocalDate today, EmployeeRecord employee)
	{
		return monthDayOf(today).equals(monthDayOf(employee.birthday()));
	}

	private boolean employeeIsBornInLeapYearTomorrow(LocalDate today, EmployeeRecord employee)
	{
		return employeeIsBornInLeapYear(employee) && todayIsFebruary28th(today);
	}

	private boolean employeeIsBornInLeapYear(EmployeeRecord employee)
	{
		return employee.birthday().isLeapYear();
	}

	private boolean todayIsFebruary28th(LocalDate today)
	{
		return monthDayOf(today).equals(FEBRUARY_28);
	}

	private MonthDay monthDayOf(LocalDate today)
	{
		MonthDay todayMonthDay = MonthDay.of(today.getMonthValue(), today.getDayOfMonth());
		return todayMonthDay;
	}
}
