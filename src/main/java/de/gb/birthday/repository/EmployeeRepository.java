package de.gb.birthday.repository;

import java.util.List;

import de.gb.birthday.model.EmployeeRecord;

public interface EmployeeRepository
{
	List<EmployeeRecord> loadAll();
}
